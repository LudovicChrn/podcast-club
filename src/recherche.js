import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import logo1 from './images/logo1.jpg';
import './App.css';

function Recherche() {
  const history = useHistory();

  const [podcast, setPodcast] = useState([{ name: 'Mort sur le Nil' }, { name: 'Le meurtre de Roger Ackroyd' },
  { name: 'Le crime de l’Orient-Express' }, { name: 'Mon petit doigt m’a dit' }, { name: 'L’Affaire Saint-Fiacre' },
  { name: 'La veuve Couderc' }, { name: 'Le charretier de la providence' }, { name: 'La neige était sale' }]);
  const [search, setSearch] = useState("");
  const [filteredpodcast, setFilteredpodcast] = useState([])


  useEffect(() => {
    setFilteredpodcast(
      podcast.filter((podcast) =>
        podcast.name.toLowerCase().includes(search.toLowerCase())
      )
    );
    console.log(podcast);
  }, [search, podcast]);

  return (
    <div>
      <nav class="header__menu">

        <ul class="header__menu__content">
          <li><a onClick={() => history.push('/')}>Accueil</a></li>
          <li><a onClick={() => history.push('/recherche')}>Recherche</a></li>
        </ul>
      </nav>
      <header className="App-header">
        <img src={logo1} className="App-logo" alt="logo" />
        <h1>PolarPodcast</h1>
        <p>
          Le nouveau podcast qui plonge les adeptes de polars au coeur d'enquêtes captivantes,<br></br>
imaginées par les plus grands romanciers du genre.
        </p>
      </header>
      <body>
        <h1>Liste des podcasts</h1>
        <input
          type="text"
          placeholder="Podcasts"
          onChange={(e) => setSearch(e.target.value)}
        />
        {filteredpodcast.map((podcast, idx) => (
          <PodcastDetail key={idx} {...podcast} />
        ))}
      </body>



    </div>
  );
};
const PodcastDetail = (props) => {
  const { name } = props;
  return (
    <>
      <p>
        <alt src={name} />
      </p>
      <p>{name}</p>
    </>
  );
}
export default Recherche;