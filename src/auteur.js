import React, { useState } from 'react';

export default function Auteur(props) {
    return (
        <div className="boxauteur">
            <div className="auteur" style={{ backgroundImage: `url(${props.img})`, backgroundSize: "cover" }}></div>
            <div className="vignette">
                <h2>{props.author}</h2>
                <p className="auteurText">{props.description}</p>
            </div>
        </div>
    );
}
