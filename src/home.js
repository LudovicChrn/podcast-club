import React from "react";
import { useHistory } from "react-router-dom";
import logo1 from "./images/logo1.jpg";
import "./App.css";
import { useState } from 'react';
import Podcast from './podcast.js';
import Auteur from './auteur.js';
import './podcast.css';
import roman1 from './images/roman1.jpg';
import roman2 from './images/roman2.jpg';
import roman3 from './images/roman3.jpg';
import roman4 from './images/roman4.jpg';
import roman5 from './images/roman5.jpg';
import roman6 from './images/roman6.jpg';
import roman7 from './images/roman7.jpg';
import roman8 from './images/roman8.jpg';
import Agatha_Christie from './images/Agatha_Christie.png';
import georges_simenon from './images/georges_simenon.JPG';

const podcast = [
  {
    id: 1,
    img: roman1,
    author: "Agatha Christie",
    title: "Mort sur le Nil",
    time: "1h36",
    smalldesc: "La riche et belle héritière américaine Linnet Ridgeway s'éprend de Simon Doyle, le fiancé de sa meilleure amie...",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  },
  {
    id: 2,
    img: roman2,
    author: "Agatha Christie",
    title: "Le meurtre de Roger Ackroyd",
    time: "1h36",
    smalldesc: "Un soir, dans sa propriété de Fernly Park, l’industriel Roger Ackroyd se confie à son ami...",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  },
  {
    id: 3,
    img: roman3,
    author: "Agatha Christie",
    title: "Le crime de l’Orient-Express",
    time: "1h36",
    smalldesc: "Hercule Poirot est en mission à Istanbul lorsqu’une urgence demande son retour à Londres...",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  },
  {
    id: 4,
    img: roman4,
    author: "Agatha Christie",
    title: "Mon petit doigt m’a dit",
    time: "1h36",
    smalldesc: "Tommy et Tuppence Beresford qui, par goût de l’aventure, fondèrent une agence de détectives privés dans leur jeunesse",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  },
  {
    id: 5,
    img: roman5,
    author: "Georges Simonon",
    title: "L'Affaire Saint-Fiacre",
    time: "1h36",
    smalldesc: "Un grattement timide à la porte ; le bruit d'un objet posé sur le plancher ; une voix furtive : « Il est ...",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  },
  {
    id: 6,
    img: roman6,
    author: "Georges Simonon",
    title: "La veuve Couderc",
    time: "1h36",
    smalldesc: "Lorsque Jean sort de prison, il est hébergé par une veuve de quarante-cinq ans dont il devient l'amant. Ancienne servante,",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  },
  {
    id: 7,
    img: roman7,
    author: "Georges Simonon",
    title: "Le Charretier de la providence",
    time: "1h36",
    smalldesc: "Des faits le plus minutieusement reconstitués, il ne dégageait rien, sinon que la découverte des deux charretiers... ",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  },
  {
    id: 8,
    img: roman8,
    author: "Georges Simonon",
    title: "La neige était sale",
    time: "1h36",
    smalldesc: "Sans un événement fortuit, le geste de Frank Friedmaier, cette nuit-là, n’aurait eu qu’une importance relative.",
    bigdesc: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
    date: "17/06/2019",
    numb: "12",
  }
]

const auteur = [
  {
    id: 9,
    img: georges_simenon,
    author: "Georges Simonon",
    description: "Ecrivain belge francophone né à Liège en Belgique le 13 février 19031 et mort à Lausanne en Suisse le 4 septembre 1989. L'abondance et le succès de ses romans policiers — dont les Maigret — éclipsent en partie le reste de son œuvre très riche : 193 romans, 158 nouvelles, plusieurs œuvres autobiographiques et de nombreux articles et reportages publiés sous son propre nom, ainsi que 176 romans, des dizaines de nouvelles, contes galants et articles parus sous 27 pseudonymes."
  },
  {
    id: 10,
    img: Agatha_Christie,
    author: "Agatha Christie",
    description: "Agatha Christie, née Agatha Mary Clarissa Miller le 15 septembre 1890 à Torquay et morte le 12 janvier 1976 à Wallingford (Oxfordshire), est une femme de lettres britannique, auteur de nombreux romans policiers. Son nom est associé à celui de ses deux héros : Hercule Poirot, détective professionnel belge, et Miss Marple, détective amateur. On la surnomme « la reine du crime ».",
  }
]

function Home() {
  const history = useHistory();
  const [statePodcast, setStatePodcast] = useState(podcast)
  const [stateAuteur, setStateAuteur] = useState(auteur)


  return (
    <div>
      <nav class="header__menu">
        <ul class="header__menu__content">
          <li>
            <a onClick={() => history.push("/")}>Accueil</a>
          </li>
          <li>
            <a onClick={() => history.push("/recherche")}>Recherche</a>
          </li>
        </ul>
      </nav>
      <header className="App-header">
        <img src={logo1} className="App-logo" alt="logo" />
        <h1>PolarPodcast</h1>
        <p>
          Le nouveau podcast qui plonge les adeptes de polars au coeur
          d'enquêtes captivantes,<br></br>
          imaginées par les plus grands romanciers du genre.
        </p>
      </header>
      <body>
        <section className="firstSection">
        <h1>Accueil</h1>
        <div className="podcast_list">
        {
          statePodcast.map((item) => {
            return (
              <Podcast
                id={item.id}
                author={item.author}
                time={item.time}
                img={item.img}
                smalldesc={item.smalldesc}
                bigdesc={item.bigdesc}
                date={item.date}
                numb={item.numb}
                title={item.title}
                item={item}
              />
            )
          })
        }
      </div>
      </section>
      <section className="secondSection">
        <h2 className="auteurTitle">Focus sur les auteurs</h2>
      <div className="auteur_list">
        {

          stateAuteur.map((item) => {
            return (
              <Auteur
                id={item.id}
                author={item.author}
                img={item.img}
                description={item.description}
              />
            );
          })
        }

      </div>
      </section>
      </body>
    </div>
  );
}

export default Home;
