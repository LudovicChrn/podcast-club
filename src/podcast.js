import React, { useState } from 'react';

export default function Podcast(props) {
	return (
    <div className="boxcast">
        <div className="cast" style={{ backgroundImage: `url(${props.img})`, backgroundSize: "cover"}}></div>
        <h2>{props.author}</h2>
        <h4>{props.title}</h4>
        <p className="smalltext">{props.smalldesc}</p>
    </div>
	);
}
