
import "./App.css";
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./home";
import Recherche from "./recherche";


function App() {
  
    
  return (
    <div className="App">

      <Router>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/recherche" exact>
            <Recherche />
          </Route>
        </Switch>
      </Router>
    </div>
    );
}       



export default App;
